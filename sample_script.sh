How to install GitBash

- Use the link provided or search for GitBash for Windows on Google.
- Click the link and once downloaded launch the installer.
- Run through the options to install until completed.
- Log into Bitbucket or create a new account.
- Click repository and create a new one.
- Name the repository and clone using the clipboard to copy.
- Open GitBash in the folder location you wish to use the repository.
- start Gitting.